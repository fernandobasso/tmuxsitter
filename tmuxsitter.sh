#!/usr/bin/env bash

#
# Disable unicode quote thing for the entire file.
#
# shellcheck disable=SC1111
#

##
# Usage help for the script.
#
function usage () {
  local name="${0##*/}"

  cat << EOF
USAGE:

  $name SESNAME WORKDIR WINNAME[:SPLIT-TYPE]...

SPLIT-TYPE is either “h” or “v”.

“h” creates horizontal windows:

  +--------------+--------------+
  |              |              |
  |              |              |
  |              |              |
  |              |              |
  |              |              |
  +--------------+--------------+


“v” creates vertical windows:

  +-----------------------------+
  |                             |
  |                             |
  +-----------------------------+
  |                             |
  |                             |
  +-----------------------------+


EXAMPLE 1:
• sesname: MyProj
• workdir: ~/SRC/MyProj
• window 1: named nvim, full pane

  $ $name MyProj ~/SRC/MyProj nvim

EXAMPLE 2:
• sesname: HtDP2eD
• workdir: ~/SRC/HtDP2eD
• window 1: named emacs, full pane
• window 2: named shell, two horizontal panes (one beside the other)

  $ $name HtDP2eD ~/SRC/HtDP2eD emacs shell:h

EXAMPLE 3:
• sesname: LOGS
• workdir: your home directory
• window 1: named logs, two vertical panes (one atop of the other)
• window 2: named shell, two horizontal panes

  $ $name LOGS ~ logs:v shell:

You can create any other combination of windows, like:

  $ $name MyStudies nvim:v manpages:h
  $ $name frontend vim jest servers:h shell:h
  $ $name Dotfiles emacs:v docs:h shell
  $ $name ServMaint sshadmin

NOTE: These names like “emacs”, “nvim”, “vim”, “shell” and others
are not commands that are executed. They are just Tmux window names.

EOF

  exit 0;
}

##
# Checks the split type, if any.
#
function get_split_type () {
  if [[ $1 =~ :h ]]; then
    echo h
  elif [[ $1 =~ :v ]]; then
    echo v
  fi
}

##
# Parses a clean name out of “name:h” or “name:v” tokens.
# If no “:” is present, simply return the value unmodified.
#
# Examples:
# • “foo” produces “foo”;
# • “foo:h” produces “foo”;
# • “foo:h:v” produces “foo”.
#
function parse_winname () {
  name="$1"
  echo "${name%%:*}"
}

##
# Build the required params for a minimal Tmux sessions.
#
# A minimal session requires:
# • a session name;
# • a work directory;
# • a window name.
#
# @param $1 nameref required_params array.
# @param $2 session name.
# @param $3 work directory.
# @param $4 window name with optional split markers.
#
function build_required_params () {
  #
  # params is required_params in ini(). It is an array to
  # store the required arguments to a minimal tmux session.
  #
  local -n params="$1"

  local sesname="$2"
  local workdir="$3"
  local split_type
  local winname

  split_type="$(get_split_type "$4")"
  winname="$(parse_winname "$4")"

  #
  # These are required. We won't go any further without them.
  #
  if [ -z "$sesname" ] || [ -z "$workdir" ] || [ -z "$winname" ]
  then
    usage
  fi

  params+=('-s' "$sesname" '-c' "$workdir")

  #
  # We don't say ‘new-window -n $sesname’ now because Tmux creates
  # a window by default when creating a new section. We just need
  # to say “-n name” for that first window.
  #
  if [[ $split_type == h ]] ; then
    params+=('-n' "$winname" ';' 'split-window' '-h')
  elif [[ "$split_type" =~ v ]] ; then
    params+=('-n' "$winname" ';' 'split-window' '-v')
  else
    params+=('-n' "$winname")
  fi
}

##
# Builds an array with Tmux window especificaiton params.
#
# Each argument should be in one of these forms:
#
# • name    Create a window with with the given name;
# • name:   Create window with the given name and a
#           horizontal split;
# • name-   Create window with the given name and a
#           vertical split.
#
function build_optional_params () {
  #
  # params is optional_args_params in main(). It is an array to
  # store the optional arguments that we use to create extra
  # windows.
  #
  local -n opt_args="$1"

  #
  # Shift away the nameref array we passed as the first argument.
  #
  shift 1

  for arg in "$@" ; do
    local split_type
    local winname
    split_type=$(get_split_type "$arg")
    winname=$(parse_winname "$arg")

    if [[ $split_type == h ]] ; then
      opt_args+=(';' 'new-window' '-n' "$winname" ';' 'split-window' '-h')
    elif [[ $split_type == v ]] ; then
      opt_args+=(';' 'new-window' '-n' "$winname" ';' 'split-window' '-v')
    else
      opt_args+=(';' 'new-window' '-n' "$winname")
    fi
  done
}

function main () {
  #
  # Used as a nameref array for storing the required arguments
  # to create a Tmux session.
  #
  local required_args

  #
  # Used as a nameref array for storing the optional arguments
  # so we can create a Tmux session with extra windows and splits.
  #
  local optional_args

  build_required_params required_args "$@"

  #
  # Shift away the required arguments we have already handled.
  #
  shift 3

  build_optional_params optional_args "$@"

  # declare -p "${required_args[@]}"

  tmux new-session "${required_args[@]}" "${optional_args[@]}"
}

main "$@"

# vim: set tw=72:

