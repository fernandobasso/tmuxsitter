# Tmuxsitter

Bash scripts to help creating Tmux sessions.

## Ideas

- Creating easy to launch pre-configured layouts that (user just provides main title and a working directory).
- Design simpler interface to create sessions with title, windows, panes, working directory, etc.
- Manage a history of the past created sessions for easily and quickly launching them again.
- Make commands searchable.


## What about Tmux Resurect?

It is an amazing tool! I just wish it could save and restore single sessions at a time as well, if needed. I constantly end up with 7 to 10 sessions saved and restored at a time, and many of them have from four to ten shell sessions running. Most of these sessions are used only from time to time. Killing those sessions before saving with Tmux Resurrect would cause me to have to do the boring task of creating them only when I need.

I sometimes write simple shell scripts for creating sections with titles, directories and other things I need, but it is not optimal an optimal workflow.

All in all, this is my attempt to find an easier solution for the “problems” described above.

## Layouts

### Single Window Layout

Simple layout with one single, full size Tmux window.

### Web front end development:
- Main window with Vim.
- Second window to run jest.
- Third window with two panes, server and shell.
- Fourth window with the entire space for shell, git, diffs, etc.

## Command Line Interface

`emacs:h` means the window name is “emacs” with one horizontal split window. It produces the following command line arguments:

```
 -n emacs \; split-window -h
```

Result:

```
  +--------------+--------------+
  |              |              |
  |              |              |
  |              |              |
  |              |              |
  |              |              |
  +--------------+--------------+
```

`emacs:v` means the window name is “emacs” and split vertically. It produces the following command line arguments:

```
tmux new-session -s emacs \; split-window -v
```

Result:

```
  +-----------------------------+
  |                             |
  |                             |
  +-----------------------------+
  |                             |
  |                             |
  +-----------------------------+
```

